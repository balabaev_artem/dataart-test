package com.bart.entity;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by bart on 27.01.2018.
 */
@Entity
@Table(name = "CARD", schema = "DATAART", catalog = "")
public class CardEntity {
    private Long id;
    private String cardNumber;
    private String pin;
    private Boolean isLocked;
    private Long wrongAttemp;
    private BigDecimal balance;

    @Id
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "CARD_NUMBER")
    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Basic
    @Column(name = "PIN")
    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    @Basic
    @Column(name = "IS_LOCKED")
    public Boolean getLocked() {
        return isLocked == null ? false : isLocked;
    }

    public void setLocked(Boolean locked) {
        isLocked = locked;
    }

    @Basic
    @Column(name = "WRONG_ATTEMP")
    public Long getWrongAttemp() {
        return wrongAttemp;
    }

    public void setWrongAttemp(Long wrongAttemp) {
        this.wrongAttemp = wrongAttemp;
    }


    @Basic
    @Column(name = "BALANCE")
    public BigDecimal getBalance() {
        return balance == null ? new BigDecimal(0) : balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CardEntity that = (CardEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (cardNumber != null ? !cardNumber.equals(that.cardNumber) : that.cardNumber != null) return false;
        if (pin != null ? !pin.equals(that.pin) : that.pin != null) return false;
        if (isLocked != null ? !isLocked.equals(that.isLocked) : that.isLocked != null) return false;
        if (wrongAttemp != null ? !wrongAttemp.equals(that.wrongAttemp) : that.wrongAttemp != null) return false;
        return balance != null ? balance.equals(that.balance) : that.balance == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (cardNumber != null ? cardNumber.hashCode() : 0);
        result = 31 * result + (pin != null ? pin.hashCode() : 0);
        result = 31 * result + (isLocked != null ? isLocked.hashCode() : 0);
        result = 31 * result + (wrongAttemp != null ? wrongAttemp.hashCode() : 0);
        result = 31 * result + (balance != null ? balance.hashCode() : 0);
        return result;
    }
}
