package com.bart.exceptions;

import org.springframework.security.core.AuthenticationException;

/**
 * Created by bart on 27.01.2018.
 */
public class NotFoundCardException extends AuthenticationException {


    public NotFoundCardException(){
        super("Карта не найдена");
    }
}
