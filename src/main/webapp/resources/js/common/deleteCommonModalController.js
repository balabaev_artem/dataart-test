/**
 * Created by chaplygin on 16.05.2016.
 */
mainModule.controller('deleteCommonModalCtrl', function($scope, $modalInstance, $log) {

	$scope.submit = function () {
		$log.debug("modal delete submit");

		$modalInstance.close(true);
	};

	$scope.cancel = function () {
		$log.debug("modal delete scancel");

		$modalInstance.dismiss(false);
	};

});