var mainModule = angular.module('mainModule', ['ui.router', 'ui.bootstrap', 'ngStorage',
    'angular-loading-bar', 'ngAnimate', 'angular-growl', 'ngMessages',
    'frapontillo.bootstrap-duallistbox', 'ui.select', 'ngSanitize', 'ae-datetimepicker', 'ncy-angular-breadcrumb',
    'smart-table', 'btorfs.multiselect', 'clear-input', 'bootstrap.fileField']);


mainModule.config(function ($stateProvider, $urlRouterProvider, $locationProvider, growlProvider, $logProvider, $provide, $breadcrumbProvider) {


    var nameProjectUrl = ctx;
    var nameProjectRes = ctx;

    var showDebugLog = true;

    $urlRouterProvider.otherwise(nameProjectUrl + '/info/');
    $stateProvider

        .state('main', {
            url: nameProjectUrl + '/main',
            abstract: true,
            template: '<ui-view/>'
        })
        .state('main.page', {
            url: nameProjectUrl + '/',
            templateUrl: nameProjectRes + '/resources/pages/home.html',
            ncyBreadcrumb: {
                label: 'Главная'
            }
        })
        .state('money', {
            url: nameProjectUrl + '/money',
            abstract: true,
            template: '<ui-view/>'
        })

        .state('money.main', {
            url: '/',
            templateUrl: nameProjectRes + '/resources/pages/money/money.html',
            controller: 'moneyController',
            ncyBreadcrumb: {
                label: 'Снятие денег'
            }
        })

        .state('money.result', {
            url: '/result?operId&balance&operSum&operDate&errmes',
            templateUrl: nameProjectRes + '/resources/pages/money/moneyResult.html',
            controller: 'moneyResultController',
            ncyBreadcrumb: {
                label: 'Статус операции'
            }
        })

        .state('balance', {
            url: nameProjectUrl + '/balance',
            abstract: true,
            template: '<ui-view/>'
        })

        .state('balance.main', {
            url: '/',
            templateUrl: nameProjectRes + '/resources/pages/balance/balance.html',
            controller: 'balanceController',
            ncyBreadcrumb: {
                label: 'Проверка баланса'
            }
        })

        .state('info', {
            url: nameProjectUrl + '/info',
            abstract: true,
            template: '<ui-view/>'
        })

        .state('info.main', {
            url: '/',
            templateUrl: nameProjectRes + '/resources/pages/balance/info.html',
            controller: 'infoController',
            ncyBreadcrumb: {
                label: 'Информация'
            }
        })


        .state('logout', {
            url: nameProjectUrl + '/logout',
            template: ' ',
            controller: 'logoutController',
            ncyBreadcrumb: {
                label: 'logout'
            }
        });


    //$locationProvider.html5Mode({
    //	enabled: true,
    //	requireBase: false
    //});

    $locationProvider.html5Mode(true);

    growlProvider.globalTimeToLive({success: 3000, error: -1, warning: 3000, info: 3000});
    growlProvider.globalReversedOrder(true);
    growlProvider.globalDisableCountDown(true);
    growlProvider.globalPosition('top-center');


    $provide.decorator('$log', function ($delegate) {
        var logLineProvider = function () {
            try {
                return new Error().stack.split("\n")[4].trim();
            } catch(err) {
                return '';
            }
        };

        var appendLineToArgs=function(args) {
            var newArgs = [];
            for (var i = 0; i < args.length; i++) {
                newArgs.push(args[i]);
            }
            newArgs.push(logLineProvider());
            return newArgs;
        };

        var decoratedLog = function decoratedLog() {
            return $delegate.apply(this, arguments);
        };

        decoratedLog.log = function dLog() {
            $delegate.log.apply($delegate,appendLineToArgs(arguments));
        };

        decoratedLog.info = function dInfo() {
            $delegate.info.apply($delegate,appendLineToArgs(arguments));
        };

        decoratedLog.warn = function dWarn() {
            $delegate.warn.apply($delegate,appendLineToArgs(arguments));
        };

        decoratedLog.error = function dError() {
            $delegate.error.apply($delegate,appendLineToArgs(arguments));
        };

        decoratedLog.debug = function dDebug() {
            $delegate.debug.apply($delegate,appendLineToArgs(arguments));
        };

        decoratedLog.$delegate = $delegate;

        return decoratedLog;
    });

    $logProvider.debugEnabled(showDebugLog);

    $breadcrumbProvider.setOptions({
        templateUrl: nameProjectRes + '/resources/pages/common/breadcrumb.html',
    });

});


mainModule.run(function ($rootScope, $sessionStorage) {
    $rootScope.nameProjectUrl = ctx;

    $("#loadImage").remove();
    $("#mainDiv").removeAttr('style');

});








