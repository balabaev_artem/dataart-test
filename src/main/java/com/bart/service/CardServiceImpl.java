package com.bart.service;

import com.bart.dto.AuthResultDTO;
import com.bart.entity.CardEntity;
import com.bart.entity.CardOperEntity;
import com.bart.enums.AuthResultEnum;
import com.bart.enums.OperTypeEnum;
import com.bart.repository.CardOperRepository;
import com.bart.repository.CardRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.Date;

import static org.springframework.util.StringUtils.hasLength;

/**
 * Created by bart on 26.01.2018.
 */
@Service
public class CardServiceImpl implements CardService {

    @Value("${max-attempt}")
    private Integer maxAtempt;

    private CardRepository cardRepository;

    private CardOperRepository cardOperRepository;

    CardServiceImpl(CardRepository cardRepository, CardOperRepository cardOperRepository){
        this.cardRepository = cardRepository;
        this.cardOperRepository = cardOperRepository;
    }


    /**
     * Проверка пинкода
     * @param card
     * @param pin
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public AuthResultDTO authorizeCard(String card, String pin){
        Assert.isTrue(hasLength(card) && hasLength(pin), "Номер карты и pin должены быть задан");
        CardEntity cardEntity = cardRepository.findByCardNumberWithLock(card);

        if (cardEntity == null)
            return new AuthResultDTO(null, AuthResultEnum.CARD_NOTFOUND);

        //если пинкод неверный
        AuthResultEnum resultEnum =  AuthResultEnum.OK;
        if(!pin.equals(cardEntity.getPin())) {
            resultEnum = AuthResultEnum.INVALID_PIN;
            Long wrongAttemp = cardEntity.getWrongAttemp() == null ? 1 : cardEntity.getWrongAttemp() + 1L;
            if (wrongAttemp >= maxAtempt) {
                cardEntity.setLocked(true);
                cardEntity.setWrongAttemp(0L);
                resultEnum = AuthResultEnum.CARD_BLOCKING;
            } else
                cardEntity.setWrongAttemp(wrongAttemp);
        //если верный пин
        } else
            cardEntity.setWrongAttemp(0L);
        cardRepository.saveAndFlush(cardEntity);

        if (cardEntity.getLocked())
            resultEnum =  AuthResultEnum.CARD_BLOCKING;



        return new AuthResultDTO(cardEntity, resultEnum);
        //return null;
    }

    /**
     * Операция просмотр остатка
     * @param cardId
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public CardOperEntity reportOper(Long cardId) {

        CardEntity cardEntity = cardRepository.findOne(cardId);
        //Assert.isTrue(!cardEntity.getLocked(), "Карта заблокирована");

        CardOperEntity cardOperEntity = new CardOperEntity();
        cardOperEntity.setCardId(cardId);
        cardOperEntity.setOperDate(new Date());
        cardOperEntity.setTypeId(OperTypeEnum.REPORT.getId());
        cardOperEntity.setOperSum(new BigDecimal(0));
        cardOperEntity.setBalance(cardEntity.getBalance());
        return cardOperRepository.saveAndFlush(cardOperEntity);
    }

    /**
     * Операция снятия наличности
     * @param cardId
     * @param sum
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public CardOperEntity outputOper(Long cardId, Double sum) {

        CardEntity cardEntity = cardRepository.findByIdWithLock(cardId);
        Assert.isTrue(!cardEntity.getLocked(), "Карта заблокирована");

        BigDecimal balance = cardEntity.getBalance().subtract(new BigDecimal(sum));
        Assert.isTrue(balance.compareTo(BigDecimal.ZERO) >= 0, "Сумма снятия превышает баланс");

        cardEntity.setBalance(balance);
        cardRepository.saveAndFlush(cardEntity);

        CardOperEntity cardOperEntity = new CardOperEntity();
        cardOperEntity.setCardId(cardId);
        cardOperEntity.setOperDate(new Date());
        cardOperEntity.setTypeId(OperTypeEnum.OUTPUT.getId());
        cardOperEntity.setOperSum(new BigDecimal(sum));
        cardOperEntity.setBalance(balance);

        return cardOperRepository.saveAndFlush(cardOperEntity);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public CardEntity save(CardEntity obj) {
        return cardRepository.saveAndFlush(obj);
    }

    @Override
    @Transactional(readOnly = true)
    public CardEntity findById(Long id) {
        return cardRepository.findOne(id);
    }


}
