mainModule.controller('balanceController', function ($scope, $http, storeService, $modal, alertService, $rootScope, $log, $state, $rootScope) {
    $log.debug("balanceController");

    $scope.balance;

    function fetchBalance() {
        var response = $http.post($rootScope.nameProjectUrl + '/api/card/balance');

        response.success(function (data) {
            //alertService.showAlertSuccess("Успешно обновлено " + data.updated + " штрафов. Ошибок: "+data.errors+" Всего получено " + data.count + " штрафов.");
            $scope.balance = data.balance;
        });

        response.error(function (data) {
            alertService.showAlertError(data);
        });
    };
    fetchBalance();
});