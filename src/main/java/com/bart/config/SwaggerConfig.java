package com.bart.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.google.common.base.Predicates.not;

/**
 * Created by bart on 27.01.2018.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(not(PathSelectors.regex("/error")))
                .build()
                //.globalOperationParameters(commonParameters())
                .pathMapping("/")
                .apiInfo(apiInfo());
    }
    private ApiInfo apiInfo() {
        ApiInfo apiInfo = new ApiInfo(
                "External Service API (Балабаев Артем)",
                "TEST...",
                "1.0",
                "Terms of service",
                new Contact("contact", "", "bart_mail@mail.ru"),
                "License of API",
                "API license URL");
        return apiInfo;
    }
}