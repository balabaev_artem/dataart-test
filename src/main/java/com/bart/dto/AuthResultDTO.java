package com.bart.dto;

import com.bart.entity.CardEntity;
import com.bart.enums.AuthResultEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by bart on 28.01.2018.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AuthResultDTO {
    private CardEntity cardEntity;
    private AuthResultEnum authResultEnum;
}
