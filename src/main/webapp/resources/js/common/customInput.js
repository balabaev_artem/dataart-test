'use strict';

(function($) {
	$.fn.phAnim = function( options ) {

		// Set default option
		var settings = $.extend({}, options),
			label,
			ph;

		// get label elem
		function getLabel(input) {
			return $(input).parent().find('label');
		}

		// move up label if we already have value in input
		function moveUpLabel(thisElement) {
			if(thisElement.val() != ''){
				getLabel(thisElement).addClass('active');
			}

			if(thisElement.parent().find("span").children().children().text() != ""){
				getLabel(thisElement).addClass('active');
			}
		}

		// generate an id
		function makeid() {
			var text = "";
			var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
			for( var i=0; i < 5; i++ )
				text += possible.charAt(Math.floor(Math.random() * possible.length));
			return text;
		}

		return this.each( function() {

			// check if the input has id or create one
			if( $(this).attr('id') == undefined ) {
				$(this).attr('id', makeid());
			}

			// check if elem has label or create one
			if( getLabel($(this)).length == 0 ) {
				// check if elem has placeholder
				if( $(this).attr('placeholder') != undefined ) {
					ph = $(this).attr('placeholder');
					$(this).attr('placeholder', '');
					// create a label with placeholder text
					$(this).parent().prepend('<label for='+ $(this).attr('id') + ' class="label-custom-input-class"' +'>'+ ph +'</label>');

					moveUpLabel($(this));
				}
			} else {
				// if elem has label remove placeholder
				$(this).attr('placeholder', '');

				moveUpLabel($(this));

				// check label for attr or set it
				if(getLabel($(this)).attr('for') == undefined ) {
					getLabel($(this)).attr('for', $(this).attr('id'));
				}
			}

			$(this).on('focus', function() {
				label = getLabel($(this));
				label.addClass('active focusIn');
			}).on('focusout', function() {
				if( $(this).val() == '') {

					if($(this).parent().attr('class').indexOf("ui-select") >= 0) {
						if($(this).parent().find("span").children().children().text() == "")
							label.removeClass('active');
					}
					else
						label.removeClass('active');

				}
				label.removeClass('focusIn');
			});
		});
	};
}(jQuery));

$(document).ready(function() {
	$('.custom-input input').phAnim();
});