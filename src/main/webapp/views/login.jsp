<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 04.05.2016
  Time: 23:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Login Page</title>
    <!-- Bootstrap 3.3.6 -->
    <link href="<c:url value="/resources/css/font-awesome-4.6.1/css/font-awesome.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/plugins/bootstrap/css/bootstrap.min.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/plugins/jQuery/jQuery-2.2.0.min.js"/>" type="text/javascript"></script>
    <script src="<c:url value="/resources/plugins/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
    <script src="<c:url value="/resources/plugins/angular/angular.min.js"/>" type="text/javascript"></script>
    <%--<script src="<c:url value="/resources/js/common/angular.1.3.14.min.js"/>" type="text/javascript"></script>--%>
    <%--<script src="<c:url value="/resources/js/common/mask.js" />"></script>--%>
    <script src="<c:url value="/resources/plugins/jQuery/jquery.maskedinput.js" />"></script>
    <script src="<c:url value="/resources/js/loginController.js"/>" type="text/javascript"></script>

</head>
<body>
<div id="content">
    <div class="container-fluid"><br/>
        <br/>

        <c:if test="${not empty error}">
            <div class="col-sm-offset-4 col-sm-4">
                <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>
                        <%--${error}--%>
                    <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}"/>.
                    <button type="button" class="close" data-dismiss="alert">×</button>

                </div>
            </div>
            <div class="row" style="margin-top: 15px">
                <div class="col-md-2 col-md-offset-5 text-center pagination-centered">
                    <%--<div class="custom-input">--%>


                                <a href="/login" type="submit" class="btn btn-success fa"  style="margin-left: 0px; " >
                                    Вернуться
                                </a>

                    <%--</div>--%>
                </div>
            </div>
        </c:if>
        <c:if test="${empty error}">
        <div class="row">
            <div class="col-sm-offset-4 col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title"><i class="fa fa-lock"></i> Данные карты</h1>
                    </div>

                    <div class="panel-body">

                            <form action="<c:url value='/login' />" method='POST'>
                                <div class="form-group">
                                    <label for="input-username">Номер карты</label>

                                    <div class="input-group"><span class="input-group-addon"><i
                                            class="fa fa-user"></i></span>
                                        <input type="text" name="username"  placeholder="ХХХХ ХХХХ ХХХХ ХХХХ"
                                               id="input-username" class="form-control"/>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <section ng-app="loginPage" ng-controller="loginCtrl">

                                        <label for="input-password">Пин код</label>

                                        <div class="input-group"><span class="input-group-addon"><i
                                                class="fa fa-lock"></i></span>
                                            <input type="{{inputType}}" name="password" value="" placeholder="Пин код"
                                                   id="input-password" class="form-control"/>
                                        </div>


                                        <%--<input type="checkbox" id="checkbox" ng-model="passwordCheckbox" ng-click="hideShowPassword()" />--%>
                                        <%--<label for="checkbox" ng-if="passwordCheckbox" style="font-weight: 100;">Скрыть пароль</label>--%>
                                        <%--<label for="checkbox" ng-if="!passwordCheckbox" style="font-weight: 100;">Показать пароль</label>--%>
                                    </section>
                                </div>

                                <div class="text-center pagination-centered">
                                    <%--<button type="submit" class="btn btn-primary"><i class="fa fa-key"></i> Войти--%>
                                    <%--</button>--%>
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-primary fa" style="width: 90px">
                                            Войти
                                        </button>
                                        <button type="button" class="btn btn-default" onclick="$('#input-password').val('')" style="width: 90px">
                                            Очистить
                                        </button>
                                    </div>
                                </div>
                                <input type="hidden" name="${_csrf.parameterName}"
                                       value="${_csrf.token}"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        </c:if>
    </div>

</body>
</html>
