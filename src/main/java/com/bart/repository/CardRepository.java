package com.bart.repository;

import com.bart.entity.CardEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import javax.persistence.QueryHint;

/**
 * Created by bart on 26.01.2018.
 */
@Repository
public interface CardRepository extends JpaRepository<CardEntity, Long>{


    /**
     * Метод с блокировкой, для избежания deadlock
     * @param cardNumber
     * @return
     */
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @QueryHints({@QueryHint(name = "javax.persistence.lock.timeout", value ="1000")})
    @Query(value = "SELECT c FROM CardEntity c  WHERE cardNumber = :cardNumber")
    CardEntity findByCardNumberWithLock(@Param("cardNumber") String cardNumber);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @QueryHints({@QueryHint(name = "javax.persistence.lock.timeout", value ="1000")})
    @Query(value = "SELECT c FROM CardEntity c  WHERE id = :cardId")
    CardEntity findByIdWithLock(@Param("cardId") Long cardId);
//
//
//    /**
//     * Обычный метод получения
//     * @param cardNumber
//     * @return
//     */
      @Query(value = "SELECT c FROM CardEntity c  WHERE cardNumber = :cardNumber")
      CardEntity findByCardNumber(@Param("cardNumber") String cardNumber);
}
