/**
 * Created by bart on 02.07.2016.
 */

mainModule.service('storeService', function ($sessionStorage, $log) {

    //-----------------------------------------------------

    // глобальная инициализация фильтров
    this.createFilter = function () {
        $log.debug('storeService: create filters tree');

        if (!$sessionStorage.filter) {
            $sessionStorage.filter = {};
        }

        // пункты меню
        this.initFilter();
    };

    this.initFilter = function () {
        if (!$sessionStorage.filter.infoblock) {
            $log.debug('storeService: recreate reports');
            this.createInfoblockFilter();
        }
    };

    this.createInfoblockFilter = function () {
        $sessionStorage.filter.infoblock = {
            code: null,
            name: null,
            infoblock: null
        };

        this.initInfoblockFilter();
    };

    this.getInfoblockFilter = function () {
        this.initInfoblockFilter();
        return $sessionStorage.filter.infoblock;
    };

    this.setInfoblockFilter = function (gcObj) {
        return $sessionStorage.filter.infoblock = gcObj;
    };

    // сбросили всех прямых потомков для ГК
    this.initInfoblockFilter = function () {
        this.createReportsFilter();
    };

    this.createReportsFilter = function () {
        $sessionStorage.filter.infoblock.reports = {
            startDate: moment().subtract(30, 'days'),
            finishDate: moment().subtract(-10, 'days')
        };

        this.initReportsFilter();
    };

    this.getReportsFilter = function () {
        this.initReportsFilter();
        return $sessionStorage.filter.infoblock.reports;
    };

    this.setReportsFilter = function (gcObj) {
        return $sessionStorage.filter.infoblock.reports = gcObj;
    };

    // сбросили всех прямых потомков для ГК
    this.initReportsFilter = function () {
    };

    // -------- utils

    this.toDate = function (date, frmt) {
        if (!frmt) {
            frmt = "DD.MM.YYYY";
        }

        if (date) {
            return moment(date).format(frmt);
        }
        return null;
    };

    this.toDateForSend = function (date, frmt) {

        if (date === '') {
            return null;
        }

        if ((!date))
            return null;

        if (!frmt) {
            frmt = "YYYY-MM-DD";
        }

        if (typeof(date) == "string")
            return moment(date, "DD.MM.YYYY").format(frmt);
        else
            return moment(date).format(frmt);
    }

});

mainModule.controller('mainController', function ($scope, $timeout, $rootScope, $log, storeService, accessService) {
    $log.debug('mainController: ');

    storeService.createFilter();

    // accessService.loadUserRoles();

    $timeout(function() {
        if(sessionStorage.href && angular.element('#' + sessionStorage.href).parents('ul.treeview-menu').length != 0) {
            var parentsClickedHref = angular.element('#' + sessionStorage.href).parents('li.treeview');

            for (var i = 0; i < parentsClickedHref.length; i++) {
                angular.element('#' + parentsClickedHref[i].firstElementChild.id).trigger('click');
            }
        }
    }, 1);

    $timeout(function() {

        if(sessionStorage.href) {
            angular.element('#'+sessionStorage.href).parent('li').addClass("active");

            var parentsClickedHref = angular.element('#' + sessionStorage.href).parents('li.treeview');

            for (var i = 0; i < parentsClickedHref.length; i++) {
                angular.element('#' + parentsClickedHref[i].firstElementChild.id).parent('li').addClass("active");
            }
        }

    }, 1000);

    /*$rootScope.previousState;
     $rootScope.currentState;

     $rootScope.$on('$stateChangeSuccess', function(ev, to, toParams, from, fromParams) {
     $rootScope.previousState = from.name;
     $rootScope.currentState = to.name;
     $rootScope.fromParams = fromParams;
     $log.debug('Previous state:'+$rootScope.previousState);
     $log.debug('Current state:'+$rootScope.currentState);




     $timeout(function() {

     $('.custom-input input').phAnim();

     }, 100);
     });*/

});