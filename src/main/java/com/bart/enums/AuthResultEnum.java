package com.bart.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bart on 26.01.2018.
 */
public enum AuthResultEnum {

    OK(1L, "OK", "Успешный вход"),
    INVALID_PIN(2L, "INVALID PIN", "Неверный пин или карта"),
    CARD_BLOCKING(3L, "CARD_BLOCKING", "Карта заблокирована"),
    CARD_NOTFOUND(4L, "CARD_NOTFOUND", "Карта не найдена");

    private Long id;
    private String name;
    private String code;

    AuthResultEnum(Long id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }
    public static AuthResultEnum getById(Long id) {
        for (AuthResultEnum type : values()) {
            if (type.id.equals(id))
                return type;
        }
        return null;
    }

    public static AuthResultEnum getByCode(String code) {
        for (AuthResultEnum type : values()) {
            if (type.code.equals(code))
                return type;
        }
        return null;
    }

    public static List<AuthResultEnum> getList() {
        List<AuthResultEnum> types = new ArrayList<>();
        for (AuthResultEnum type : values()) {
            types.add(type);
        }
        return types;
    }

    public static Map<Long, AuthResultEnum> getMapById() {
        Map<Long, AuthResultEnum> types = new HashMap<>();
        for (AuthResultEnum type : values()) {
            types.put(type.id, type);
        }
        return types;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
