package com.bart.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bart on 26.01.2018.
 */
public enum OperTypeEnum {

    OUTPUT(2L, "OUTPUT", "Снятие"),
    REPORT(1L, "REPORT", "Просмотр остатка");

    private Long id;
    private String name;
    private String code;

    OperTypeEnum(Long id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }
    public static OperTypeEnum getById(Long id) {
        for (OperTypeEnum type : values()) {
            if (type.id.equals(id))
                return type;
        }
        return null;
    }

    public static OperTypeEnum getByCode(String code) {
        for (OperTypeEnum type : values()) {
            if (type.code.equals(code))
                return type;
        }
        return null;
    }

    public static List<OperTypeEnum> getList() {
        List<OperTypeEnum> types = new ArrayList<>();
        for (OperTypeEnum type : values()) {
            types.add(type);
        }
        return types;
    }

    public static Map<Long, OperTypeEnum> getMapById() {
        Map<Long, OperTypeEnum> types = new HashMap<>();
        for (OperTypeEnum type : values()) {
            types.put(type.id, type);
        }
        return types;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
