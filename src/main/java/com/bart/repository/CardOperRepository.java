package com.bart.repository;

import com.bart.entity.CardOperEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by bart on 26.01.2018.
 */
@Repository
public interface CardOperRepository extends JpaRepository<CardOperEntity, Long> {
}
