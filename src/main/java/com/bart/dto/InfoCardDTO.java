package com.bart.dto;

import com.bart.entity.CardEntity;
import com.bart.session.SessionStorage;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;


/**
 * Created by bart on 29.01.2018.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class InfoCardDTO {

    private String cardNumber;
//    @JsonFormat
//            (shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private Date currentDate;

    
}
