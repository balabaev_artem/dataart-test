mainModule.controller('logoutController', function ($scope, $window, $http, $rootScope, alertService) {
    'use strict';

    $http.post($rootScope.nameProjectUrl+'/logout', null).success(function () {
        $window.location.href = $rootScope.nameProjectUrl;
    }).error(function () {
        alertService.showAlertError(data);
    });

});