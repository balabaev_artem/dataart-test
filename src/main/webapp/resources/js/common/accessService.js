/**
 * Created by cane on 21.07.16.
 */
mainModule.service('accessService', function ($log, $http, $rootScope, $sessionStorage, $stateParams) {

    var availableRoles = [];

    this.loadUserRoles = function () {
        $http.get($rootScope.nameProjectUrl + '/api/users/roles')
            .then(
                // success
                function (resp) {
                    $log.debug("users pages   " + resp.data);
                    availableRoles = resp.data;
                    $rootScope.isReadOnly = availableRoles.some(function (element) {
                        return element === 'READ_ONLY';
                    });
                },
                // error
                function (err) {
                    $log.info(err);
                });
    };

    this.checkAccess = function (role) {
        return availableRoles.some(function (element) {
            return element == role;
        });
    };
    
});

