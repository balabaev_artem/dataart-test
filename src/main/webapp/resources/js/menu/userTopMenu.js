mainModule.directive('userTopMenu', function ($rootScope, alertService, $http, $modal) {
    'use strict';
    return {
        restrict: 'A',
        templateUrl: ctx + '/resources/pages/menu/userTopMenu.html',
        link: function (scope) {
            scope.user = {
                name: "Developer"
            }
        }
    };
});