package com.bart;

import com.bart.entity.CardEntity;
import com.bart.entity.OperTypeEntity;
import com.bart.repository.CardRepository;
import com.bart.repository.OperTypeRepository;
import com.bart.service.CardService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.smartcardio.Card;

@RunWith(SpringRunner.class)
@SpringBootTest
@EntityScan( basePackages = {"com.bart.entity"} )
public class CardTerminalApplicationTests {

	@Autowired
	CardService cardService;

	@Autowired
	CardRepository cardRepository;

	@Autowired
	OperTypeRepository operTypeRepository;

	String cardNumber = "!111!";
	String pinCode = "!111!";



	public CardEntity createCard(){
		CardEntity cardEntity = new CardEntity();
        cardEntity.setId(0L);
		cardEntity.setCardNumber(cardNumber);
		cardEntity.setPin(pinCode);
		return cardRepository.save(cardEntity);

	}

	@Test
	@Transactional
	public void TestLock() {
		createCard();
		CardEntity obj = cardRepository.findByCardNumberWithLock(cardNumber);
		Assert.isTrue(obj != null, "Не найдена карта");

	}

	@Test
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void reportOperation(){
		CardEntity card = createCard();
		cardService.outputOper(card.getId(), -200d);

	}

}
