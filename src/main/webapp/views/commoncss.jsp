<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<!-- Skins. Choose a skin from the css/skins
folder instead of downloading all of them to reduce the load. -->
<link href="<c:url value="/resources/css/skins/skin-blue.min.css" />" rel="stylesheet">


<link href="<c:url value="/resources/images/favicon.ico" />" rel="icon" type="image/x-icon">

<link href="<c:url value="/resources/plugins/loadingbar/loading-bar.css" />" rel="stylesheet">

<link href="<c:url value="/resources/css/font-awesome-4.6.1/css/font-awesome.min.css" />" rel="stylesheet">

<link href="<c:url value="/resources/plugins/bootstrap-duallistbox/bootstrap-duallistbox.min.css" />" rel="stylesheet">


<%-- angular growl --%>
<link href="<c:url value="/resources/plugins/angular-growl/angular-growl.css" />" rel="stylesheet">

<!-- Tell the browser to be responsive to screen width -->

<!-- Bootstrap 3.3.6 -->
<link href="<c:url value="/resources/plugins/bootstrap/css/bootstrap.min.css" />" rel="stylesheet">

<link href="<c:url value="/resources/plugins/select2/css/select2.css" />" rel="stylesheet">

<link href="<c:url value="/resources/plugins/select2/select2-bootstrap.css" />" rel="stylesheet">

<link href="<c:url value="/resources/plugins/angular-ui-select/select.min.css" />" rel="stylesheet">

<link href="<c:url value="/resources/plugins/tooltip/tooltipster.bundle.min.css" />" rel="stylesheet">
<link href="<c:url value="/resources/plugins/tooltip/tooltipster-sideTip-shadow.min.css" />" rel="stylesheet">

<%--bootstrap datetimepicker--%>
<link href="<c:url value="/resources/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" />"
      rel="stylesheet">

<!-- Font Awesome -->
<!-- Theme style -->
<link href="<c:url value="/resources/css/ARMD.min.css" />" rel="stylesheet">

<link href="<c:url value="/resources/css/statuses.css" />" rel="stylesheet">


