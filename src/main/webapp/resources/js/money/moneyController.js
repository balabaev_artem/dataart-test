mainModule.controller('moneyController', function ($scope, $http, storeService, $modal, alertService, $rootScope, $log, $state, $rootScope) {
    $log.debug("moneyController");

    $scope.moneySum = "";
    $scope.data = {};

    $scope.putKey = function (key) {
        $scope.moneySum = $scope.moneySum  + key;
    }

    $scope.clearSum = function () {
        $scope.moneySum = $scope.moneySum.substr(0, $scope.moneySum.length - 1);
    }

    $scope.submit = function() {
        var param = {};
        param.sum = $scope.moneySum;
        var response = $http.post($rootScope.nameProjectUrl + '/api/card/output', param);

        response.success(function (data) {
            $state.go('money.result', {
                operId: data.id,
                balance: data.balance,
                operSum: data.operSum,
                operDate: data.operDate,
                errmes:null
            });
        });

        response.error(function (data) {
            $state.go('money.result', {
                errmes:data.message
            });
        });
    };


});