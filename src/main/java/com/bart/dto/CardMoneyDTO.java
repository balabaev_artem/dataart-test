package com.bart.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by bart on 27.01.2018.
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class CardMoneyDTO {
    private Double sum;
}
