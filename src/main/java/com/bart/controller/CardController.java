package com.bart.controller;

import com.bart.dto.CardMoneyDTO;
import com.bart.dto.InfoCardDTO;
import com.bart.entity.CardEntity;
import com.bart.entity.CardOperEntity;
import com.bart.entity.OperTypeEntity;
import com.bart.repository.OperTypeRepository;
import com.bart.service.CardService;
import com.bart.session.SessionStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by bart on 27.01.2018.
 *
 */

@RestController
@RequestMapping("/api/card")
public class CardController {

    @Autowired
    private CardService cardService;

    @Autowired
    private SessionStorage sessionStorage;

    /**
     * Снятие денег
     * @return
     */
    @PostMapping(value = "/output")
    public CardOperEntity output(@RequestBody CardMoneyDTO dto){

        CardOperEntity cardOperEntity = cardService.outputOper(sessionStorage.getCardId(), dto.getSum()) ;
        return cardOperEntity;
    }

    /**
     * Проверка баланса
     * @return
     */
    @PostMapping(value = "/balance")
    public CardOperEntity balance(){
        CardOperEntity cardOperEntity = cardService.reportOper(sessionStorage.getCardId()) ;
        return cardOperEntity;
    }

    /**
     * Проверка баланса
     * @return
     */
    @GetMapping(value = "/info")
    public InfoCardDTO info(){

        return new InfoCardDTO(sessionStorage.getCardNumber(), new Date());
    }


}
