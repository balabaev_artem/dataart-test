mainModule.controller('moneyResultController', function ($scope, $http, storeService, $modal, alertService, $stateParams, $rootScope, $log, $state, $rootScope) {
    $log.debug("moneyResultController");


    $scope.data = {};
    $scope.operDate;

    function printInfo() {
        $scope.data.balance = $stateParams.balance;
        $scope.data.operId = $stateParams.operId;
        $scope.data.errmes = $stateParams.errmes;
        $scope.data.operSum = $stateParams.operSum;
        $scope.operDate = moment( $stateParams.operDate, 'x').format('YYYY-MM-DD HH:mm:ss');

    };
    printInfo();


});