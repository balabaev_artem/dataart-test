package com.bart.exceptions;

import org.springframework.security.core.AuthenticationException;

/**
 * Created by bart on 27.01.2018.
 */
public class BadPinException extends AuthenticationException {


    public BadPinException(){
        super("Неверная карта или пинкод");
    }
}
