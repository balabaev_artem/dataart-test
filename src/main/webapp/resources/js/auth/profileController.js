/**
 * Created by aspasibu on 19.05.2016.
 */

mainModule.controller('profileController', function ($scope, $modal, $http, alertService, changePass, $rootScope, $log) {
    'use strict';

    $http.get($rootScope.nameProjectUrl + '/profile/info').success(function (data) {
        if (data.name) {
            $scope.userLogin = data.login;
            $scope.userId = data.id;
            $scope.userName = data.name;
            $scope.userAgencyName = data.agencyName;
            $scope.isAdminAgency = data.isAdminAgency === 1;
            $scope.isShowProfile = data.isShowProfile === 1;
        }
    }).error(function () {
        alertService.showAlertError(data);
    });

    $scope.open = function () {
        changePass.open($scope.userId);
    };

    $scope.submit = function () {

        var isShowProfile = null;

        if ($scope.isShowProfile) {
            isShowProfile = 1;
        }

        var dataObj = {
            id: $scope.userId,
            isShowProfile: isShowProfile
        };

        var response = $http.post($rootScope.nameProjectUrl + '/api/users/update-dto', dataObj);

        response.success(function (data) {
            $log.debug("data   " + data);
            $rootScope.updateUserInfo();
            alertService.showAlertSuccess("Операция завершена успешно.");

        });
        response.error(function (data) {
            alertService.showAlertError(data);
        });
    };

});