package com.bart.exceptions;

import org.springframework.security.core.AuthenticationException;

/**
 * Created by bart on 27.01.2018.
 */
public class LockingCardException extends AuthenticationException {


    public LockingCardException(){
        super("Карта заблокирована");
    }
}
