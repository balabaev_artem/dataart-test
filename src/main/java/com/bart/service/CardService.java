package com.bart.service;

import com.bart.dto.AuthResultDTO;
import com.bart.entity.CardEntity;
import com.bart.entity.CardOperEntity;
import com.bart.exceptions.BadPinException;

/**
 * Created by bart on 26.01.2018.
 */
public interface CardService {

    /**
     * Найти карту по номеру и пину
     * @param card
     * @param pin
     * @return
     */
    AuthResultDTO authorizeCard(String card, String pin) throws BadPinException;


    /**
     * Отметка о просмотре карты
     * @param cardId
     * @return
     */
    CardOperEntity reportOper(Long cardId);


    /**
     * Снятие наличности
     * @param cardId
     * @param sum
     * @return
     */
    CardOperEntity outputOper(Long cardId, Double sum);


    CardEntity save(CardEntity obj);

    CardEntity findById(Long id);
}
