package com.bart.security;

import com.bart.dto.AuthResultDTO;
import com.bart.entity.CardEntity;
import com.bart.enums.AuthResultEnum;
import com.bart.exceptions.BadPinException;
import com.bart.exceptions.LockingCardException;
import com.bart.exceptions.NotFoundCardException;
import com.bart.service.CardService;
import com.bart.session.SessionStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.persistence.LockTimeoutException;
import java.util.Collection;
import java.util.LinkedList;

/**
 * Created by bart on 27.01.2018.
 */
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private CardService cardService;

    @Autowired
    private SessionStorage sessionStorage;

    static final Logger logger =  LoggerFactory.getLogger(CustomAuthenticationProvider.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        final String cardNumber = authentication.getName().replace(" ",  "");
        final String pin = (String) authentication.getCredentials();

        AuthResultDTO authResultDTO = null;

        try {
            authResultDTO = cardService.authorizeCard(cardNumber, pin);
        }catch (Exception e){
            throw new BadCredentialsException(e.getMessage());
        }

        CardEntity cardEntity = authResultDTO.getCardEntity();
        AuthResultEnum authResult = authResultDTO.getAuthResultEnum();

        if (cardEntity == null ||authResult == AuthResultEnum.CARD_NOTFOUND){
            throw new NotFoundCardException();
        } else if ( authResult == AuthResultEnum.INVALID_PIN)
            throw new BadPinException();
        else if (authResult == AuthResultEnum.CARD_NOTFOUND){
            throw new NotFoundCardException();        }
        else if (authResult == authResult.CARD_BLOCKING)
            throw new LockingCardException();
        else if (authResult != authResult.OK)
            throw new BadCredentialsException("Неизвестный статус");


        sessionStorage.setCardId(cardEntity.getId());
        sessionStorage.setCardNumber(cardEntity.getCardNumber());

        Collection<? extends GrantedAuthority> authorities = new LinkedList<>();
        return new UsernamePasswordAuthenticationToken(new User(cardNumber, "", authorities), "", authorities);

    }

    public boolean supports(Class<?> aClass) {
        return true;
    }
}