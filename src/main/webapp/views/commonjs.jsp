<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%--moment js - Parse, validate, manipulate, and display dates in JavaScript. --%>
<script src="<c:url value="/resources/plugins/moment/moment-with-locales.min.js" />"></script>

<!-- jQuery 2.2.0 -->
<script src="<c:url value="/resources/plugins/jQuery/jQuery-2.2.0.min.js" />"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<c:url value="/resources/plugins/jQuery/jquery-ui.min.js" />"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<c:url value="/resources/plugins/bootstrap/js/bootstrap.min.js" />"></script>


<!-- Slimscroll -->
<script src="<c:url value="/resources/plugins/slimScroll/jquery.slimscroll.min.js" />"></script>
<!-- FastClick -->
<script src="<c:url value="/resources/plugins/fastclick/fastclick.js" />"></script>
<!-- App -->
<%--<script src="<c:url value="/resources/js/app.min.js" />"></script>--%>
<script src="<c:url value="/resources/js/app.js" />"></script>


<script src="<c:url value="/resources/plugins/angular/angular.min.js" />"></script>
<script data-require="angular-route@1.4.3" data-semver="1.4.3"
        src="<c:url value="/resources/plugins/angular/angular-route.js" />"></script>
<script src="<c:url value="/resources/plugins/angular/angular-animate.min.js" />"></script>
<script src="<c:url value="/resources/plugins/angular/angular-locale_ru-ru.js" />"></script>

<script src="<c:url value="/resources/plugins/ngStorage/ngStorage.js" />"></script>

<script src="<c:url value="/resources/plugins/loadingbar/loading-bar.js" />"></script>

<%--<script src="<c:url value="/resources/plugins/bootstrap/js/ui-bootstrap-tpls-0.11.0.js" />"></script>--%>
<script src="<c:url value="/resources/plugins/bootstrap/js/ui-bootstrap-tpls-0.13.4.js" />"></script>


<%--<script src="<c:url value="/resources/plugins/select2/js/select2.js"/>"></script>--%>
<%--<script src="<c:url value="/resources/plugins/select2/js/i18n/ru.js"/>"></script>--%>

<%--lodash--%>
<script src="<c:url value="/resources/plugins/lodash/lodash.core.min.js"/>"></script>

<%-- angular growl --%>
<script src="<c:url value="/resources/plugins/angular-growl/angular-growl.js" />"></script>

<%-- angulra ui router --%>
<script src="<c:url value="/resources/plugins/angular-ui-rout/angular-ui-router.min.js" />"></script>

<%-- bootstrap duallistbox --%>
<script src="<c:url value="/resources/plugins/bootstrap-duallistbox/jquery.bootstrap-duallistbox.min.js" />"></script>
<script src="<c:url value="/resources/plugins/angular/angular-bootstrap-duallistbox.min.js" />"></script>

<script src="<c:url value="/resources/plugins/angular/angular-sanitize.min.js" />"></script>
<script src="<c:url value="/resources/plugins/angular-ui-select/select.min.js" />"></script>
<script src="<c:url value="/resources/plugins/angular-messages/angular-messages.js" />"></script>


<script src="<c:url value="/resources/plugins/angular-breadcrumbs/angular-breadcrumb.min.js" />"></script>

<%--bootstrap datetimepicker--%>
<script src="<c:url value="/resources/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" />"></script>
<script src="<c:url value="/resources/plugins/bootstrap-datetimepicker/js/angular-eonasdan-datetimepicker.min.js" />"></script>

<script src="<c:url value="/resources/plugins/smart-table/smart-table.min.js" />"></script>
<script src="<c:url value="/resources/plugins/smart-table/toggle-column.js" />"></script>

<%--<script src="<c:url value="/resources/plugins/angular-ya-map/ya-map-2.1.min.js" />"></script>--%>

<%--<script src="<c:url value="/resources/plugins/tooltip/angular-tooltipster.js" />"></script>--%>
<%--<script src="<c:url value="/resources/plugins/tooltip/tooltipster.bundle.min.js" />"></script>--%>

<%-- FOR ARTEM --%>
<%-- раскомментировать для создания war пакета --%>
<%--<script src="<c:url value="/resources/scripts/application.js" />"></script>--%>

<script src="<c:url value="/resources/plugins/angular-multiselect/angular-bootstrap-multiselect.js" />"></script>

<script src="<c:url value="/resources/plugins/angular-clear-input/clear-input.js" />"></script>

<script src="<c:url value="/resources/plugins/angular-bootstrap-file-field/angular-bootstrap-file-field.js" />"></script>

<script src="<c:url value="/resources/js/mainController.js?v=1.0" />"></script>
<script src="<c:url value="/resources/js/common/deleteCommonModalController.js" />"></script>
<script src="<c:url value="/resources/js/filters/highlightTextFilter.js" />"></script>
<script src="<c:url value="/resources/js/ng-enter.js" />"></script>
<script src="<c:url value="/resources/js/common/showAlertsService.js" />"></script>
<script src="<c:url value="/resources/js/common/storeService.js" />"></script>
<script src="<c:url value="/resources/js/common/accessService.js" />"></script>
<script src="<c:url value="/resources/js/common/downloadFile.js" />"></script>
<script src="<c:url value="/resources/js/common/customInput.js" />"></script>
<%--<script src="<c:url value="/resources/js/common/mask.js" />"></script>--%>

<script src="<c:url value="/resources/js/menu/userTopMenu.js" />"></script>
<script src="<c:url value="/resources/js/auth/logoutController.js" />"></script>

<script src="<c:url value="/resources/js/money/moneyController.js" />"></script>
<script src="<c:url value="/resources/js/money/moneyResultController.js" />"></script>
<script src="<c:url value="/resources/js/balance/balanceController.js" />"></script>
<script src="<c:url value="/resources/js/balance/infoController.js" />"></script>

