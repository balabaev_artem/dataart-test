mainModule.controller('infoController', function ($scope, $http, storeService, $modal, alertService, $rootScope, $log, $state, $rootScope) {
    $log.debug("infoController");

    $scope.data;
    $scope.currDate;

    function fetchBalance() {
        var response = $http.get($rootScope.nameProjectUrl + '/api/card/info');

        response.success(function (data) {
            //alertService.showAlertSuccess("Успешно обновлено " + data.updated + " штрафов. Ошибок: "+data.errors+" Всего получено " + data.count + " штрафов.");
            $scope.data = data;
            $scope.currDate = moment( $scope.data.currentDate, 'x').format('YYYY-MM-DD HH:mm:ss');

            // input = $scope.data.currentDate
            // var day = input.dayOfMonth;
            // var month = input.monthValue - 1; // Month is 0-indexed
            // var year = input.year;
            // var hour = input.hour;
            // var minute = input.minute;
            // var second = input.second;
            //
            // $scope.currDate = moment(new Date(year, month, day, hour, minute, second)).format('YYYY-MM-DD HH:mm:ss');

        });

        response.error(function (data) {
            alertService.showAlertError(data);
        });
    };
    fetchBalance();
});