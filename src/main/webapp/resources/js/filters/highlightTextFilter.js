/**
 * Created by chaplygin on 30.05.2016.
 */
mainModule.filter('highlightTextFilter', function($sce, $rootScope){

	return function(text, search){

		if(search) {
			if (typeof text === 'string') {
				return $sce.trustAsHtml(text.replace(new RegExp(search, 'gi'), '<span class="highlightedText">$&</span>'));
			}
			return $sce.trustAsHtml(text.toString().replace(new RegExp(search, 'gi'), '<span class="highlightedText">$&</span>'));
		}
		else {
			return text;
		}
	}
});