<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@ page session="false" %>

<!DOCTYPE html>
<html lang="en" ng-app="mainModule">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="cache-control" content="max-age=0, must-revalidate"/>
    <meta http-equiv="cache-control" content="no-cache, must-revalidate"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="Pragma" content="no-cache">
    <title>Интеграции</title>

    <base href="/"/>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">


    <jsp:include page="commoncss.jsp"/>


</head>
<script>var ctx = "${pageContext.request.contextPath}"</script>
<body class="hold-transition skin-blue sidebar-mini">

<div id="loadImage" style="position: fixed; top: 50%; left: 50%; padding-left: 30px" class="loading">
    <%--<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>--%>
    <%--<span>Loading...</span>--%>
</div>

<div class="wrapper" ng-controller="mainController" id="mainDiv" style="visibility: hidden;">
    <div growl limit-messages="1"></div>
    <header class="main-header">
        <!-- Logo -->
        
    </header>

    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <ul class="sidebar-menu">
                <li class="treeview">
                    <a ui-sref="info.main" id="mainmenu">
                        <i class="fa fa-home"></i> <span>Информация</span>
                    </a>
                </li>
                <li class="treeview">
                    <a ui-sref="balance.main" id="balance-menu">
                        <i class="fa fa-btc"></i> <span>Баланс</span>
                    </a>
                </li>
                <li class="treeview">
                    <a ui-sref="money.main" id="operationsmenu">
                        <i class="fa fa-money"></i> <span>Снятие денег</span>
                    </a>
                </li>

                <li class="treeview">
                    <a ui-sref="logout" id="exit">
                        <i class="fa fa-sign-out"></i> <span>Выйти</span>
                    </a>
                </li>

            </ul>
        </section>
        <!-- sidebar -->
    </aside>

    <!-- Content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">

            <!-- Main row -->
            <div class="row">

                <div ncy-breadcrumb style="margin-top: 1px;"></div>

                <div ui-view></div>
            </div>
            <!-- /.row (main row) -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer" style="padding: 5px">
        Версия банкомата 0.0.1
        <!--div class="pull-right hidden-xs">

        </div-->
    </footer>

    <aside class="control-sidebar control-sidebar-dark">
        <div class="tab-content">
            <div class="tab-pane" id="control-sidebar-home-tab">
            </div>
            <span></span>
        </div>
    </aside>

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>

</div>


<jsp:include page="commonjs.jsp"/>


<%--<script type="text/javascript">--%>
<%--$.widget.bridge('uibutton', $.ui.button);--%>
<%--$.fn.select2.defaults.set("theme", "bootstrap");--%>
<%--</script>--%>


</body>
</html>
