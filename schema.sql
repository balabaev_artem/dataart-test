--
CREATE TABLE CARD
(
  ID            NUMBER(19)                      NOT NULL,
  CARD_NUMBER   VARCHAR2(16 BYTE)               NOT NULL,
  PIN           VARCHAR2(25 BYTE)               NOT NULL,
  IS_LOCKED     NUMBER(1),
  WRONG_ATTEMP  NUMBER(2),
  BALANCE       NUMBER(12,2)
);

COMMENT ON TABLE CARD IS '�����';

COMMENT ON COLUMN CARD.ID IS 'ID';

COMMENT ON COLUMN CARD.CARD_NUMBER IS '����� �����';

COMMENT ON COLUMN CARD.PIN IS '���-���';

COMMENT ON COLUMN CARD.IS_LOCKED IS '������� ����������: 1-�����������';

COMMENT ON COLUMN CARD.WRONG_ATTEMP IS '���-�� �������� �������';

COMMENT ON COLUMN CARD.BALANCE IS '������� ������';



--
-- OPER_TYPE  (Table) 
--
CREATE TABLE OPER_TYPE
(
  ID    NUMBER(19)                              NOT NULL,
  CODE  VARCHAR2(50 BYTE)                       NOT NULL,
  NAME  VARCHAR2(200 BYTE)                      NOT NULL
);

COMMENT ON TABLE OPER_TYPE IS '��� ��������';

COMMENT ON COLUMN OPER_TYPE.CODE IS '���������� ���';

COMMENT ON COLUMN OPER_TYPE.NAME IS '��������';



--
-- CARD_OPER_SEQ  (Sequence) 
--
CREATE SEQUENCE CARD_OPER_SEQ
  START WITH 61
  MAXVALUE 999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;


--
-- CARD_SEQ  (Sequence) 
--
CREATE SEQUENCE CARD_SEQ
  START WITH 21
  MAXVALUE 999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;


--
-- OPER_TYPE_PK  (Index) 
--
CREATE UNIQUE INDEX OPER_TYPE_PK ON OPER_TYPE
(ID);


--
-- IX_CARD_01  (Index) 
--
CREATE UNIQUE INDEX IX_CARD_01 ON CARD
(CARD_NUMBER);



--
-- CARD_PK  (Index) 
--
CREATE UNIQUE INDEX CARD_PK ON CARD
(ID);


--
-- CARD_TRG  (Trigger) 
--
CREATE OR REPLACE TRIGGER CARD_TRG
BEFORE INSERT
ON CARD
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
-- For Toad:  Highlight column ID
  IF :new.ID IS NULL THEN
    :new.id := CARD_SEQ.nextval;
  END IF;
  
  :new.is_locked := nvl(:new.is_locked, 0);
  :new.wrong_attemp := nvl(:new.wrong_attemp, 0); 
  
END CARD_TRG;
/


--
-- CARD_OPER  (Table) 
--
CREATE TABLE CARD_OPER
(
  ID         NUMBER(19)                         NOT NULL,
  TYPE_ID    NUMBER(19)                         NOT NULL,
  CARD_ID    NUMBER(19)                         NOT NULL,
  OPER_DATE  DATE                               NOT NULL,
  OPER_SUM   NUMBER(12,2),
  BALANCE    NUMBER(12,2)
);

COMMENT ON TABLE CARD_OPER IS '�������� �� �����';

COMMENT ON COLUMN CARD_OPER.TYPE_ID IS '������, ��� ��������';

COMMENT ON COLUMN CARD_OPER.CARD_ID IS '������, �����';

COMMENT ON COLUMN CARD_OPER.OPER_DATE IS '���� ��������';

COMMENT ON COLUMN CARD_OPER.OPER_SUM IS '����� ��������';

COMMENT ON COLUMN CARD_OPER.BALANCE IS '������';



--
-- CARD_OPER_PK  (Index) 
--
CREATE UNIQUE INDEX CARD_OPER_PK ON CARD_OPER
(ID);


--
-- IX_CARD_OPER_01  (Index) 
--
CREATE INDEX IX_CARD_OPER_01 ON CARD_OPER
(CARD_ID, OPER_DATE);


--
-- IX_CARD_OPER_02  (Index) 
--
CREATE INDEX IX_CARD_OPER_02 ON CARD_OPER
(TYPE_ID);


--
-- CARD_OPER_TRG  (Trigger) 
--
CREATE OR REPLACE TRIGGER CARD_OPER_TRG
BEFORE INSERT
ON CARD_OPER
REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
BEGIN
-- For Toad:  Highlight column ID
  IF :new.id IS NULL THEN
    :new.ID := CARD_OPER_SEQ.nextval;
  END IF;  
END CARD_OPER_TRG;
/


-- 
-- Non Foreign Key Constraints for Table CARD 
-- 
ALTER TABLE CARD ADD (
  CONSTRAINT CARD_PK
  PRIMARY KEY
  (ID)
  USING INDEX CARD_PK
  ENABLE VALIDATE);


-- 
-- Non Foreign Key Constraints for Table OPER_TYPE 
-- 
ALTER TABLE OPER_TYPE ADD (
  CONSTRAINT OPER_TYPE_PK
  PRIMARY KEY
  (ID)
  USING INDEX OPER_TYPE_PK
  ENABLE VALIDATE);


-- 
-- Non Foreign Key Constraints for Table CARD_OPER 
-- 
ALTER TABLE CARD_OPER ADD (
  CONSTRAINT CARD_OPER_PK
  PRIMARY KEY
  (ID)
  USING INDEX CARD_OPER_PK
  ENABLE VALIDATE);


-- 
-- Foreign Key Constraints for Table CARD_OPER 
-- 
ALTER TABLE CARD_OPER ADD (
  CONSTRAINT CARD_OPER_02_FK 
  FOREIGN KEY (TYPE_ID) 
  REFERENCES OPER_TYPE (ID)
  ENABLE VALIDATE);

ALTER TABLE CARD_OPER ADD (
  CONSTRAINT CARD_OPER_01_FK 
  FOREIGN KEY (CARD_ID) 
  REFERENCES CARD (ID)
  ENABLE VALIDATE);
  
/

Insert into OPER_TYPE
   (ID, CODE, NAME)
 Values
   (1, 'REPORT', '�������� �������');
Insert into OPER_TYPE
   (ID, CODE, NAME)
 Values
   (2, 'OUTPUT', '������');
COMMIT;
/

Insert into CARD
   (ID, CARD_NUMBER, PIN, IS_LOCKED, WRONG_ATTEMP, 
    BALANCE)
 Values
   (1, '1111111111111111', '1', 0, 0, 
    1000);
Insert into CARD
   (ID, CARD_NUMBER, PIN, IS_LOCKED, WRONG_ATTEMP, 
    BALANCE)
 Values
   (2, '2222222222222222', '2', 0, 0, 
    1000);
COMMIT;
/
  