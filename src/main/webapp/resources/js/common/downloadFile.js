/**
 * Created by bart on 11.07.2016.
 */

mainModule.service("downloadFile", function ($log) {

    this.download = function (data, status, headers) {
        headers = headers();

        var contentType = headers['content-type'];

        var content = headers['content-disposition'];
        var result = content.split(';')[1].trim().split('=')[1];
        var filename = decodeURIComponent(result.replace(/"/g, ''));

       // var linkElement = document.createElement('a');
        try {
            //var blob = new Blob([data], { type: contentType });
            //var url = window.URL.createObjectURL(blob);

            var url = URL.createObjectURL(new Blob([data]));
            var a = document.createElement('a');
            a.href = url;
            a.download = filename;
            a.target = '_blank';
            a.click();

            //linkElement.setAttribute('href', url);
            //linkElement.setAttribute("download", filename);
            //
            //var clickEvent = new MouseEvent("click", {
            //    "view": window,
            //    "bubbles": true,
            //    "cancelable": false
            //});
            //linkElement.dispatchEvent(clickEvent);
        } catch (ex) {
            $log.error(ex);
        }
    }

});