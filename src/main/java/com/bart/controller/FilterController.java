package com.bart.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by bart on 27.01.2018.
 */

/**
 * Работа F5 для ангуляра
 */
@Controller
public class FilterController {

    @RequestMapping(value = "/*", method = RequestMethod.GET)
    public String getHome() {
        return "forward:/";
    }

}