/** shared service for changing password */
mainModule.factory('changePass', function ($http, $modal, alertService, $rootScope, $log) {
	var changePass = {};

	changePass.open = function (userId) {
		$log.debug("change password dialog");
		$modal.open({
			templateUrl: $rootScope.nameProjectUrl+'/resources/pages/auth/changePasswordForm.html',
			controller: function ($scope, $http, $modalInstance) {
				$scope.data = {
					pass: "",
					pass2: "",
					errorMessage: ""
				};
				$scope.onChange = function () {
					if ($scope.data.pass2) {
						$scope.data.errorMessage = matchPasswords($scope.data.pass, $scope.data.pass2);
					} else {
						$scope.data.errorMessage = "";
					}
				};
				$scope.submit = function () {
					$log.debug("modal change pass submit");
					if ($scope.data.errorMessage = matchPasswords($scope.data.pass, $scope.data.pass2)) {
						return
					} else {
						$modalInstance.close($scope.data.pass);
					}
				};

				$scope.cancel = function () {
					$log.debug("modal change pass cancel");

					$modalInstance.dismiss(false);
				};

				function matchPasswords(pass, pass2) {
					if (pass != pass2) {
						return "Пароли не совпадают"
					} else {
						return ""
					}
				}

			}
		}).result.then(function (val) {
				$log.debug("modal return: " + val);

				if (val) {
					var response = $http.post($rootScope.nameProjectUrl+'/api/users/' + userId + '/change-pass', val);

					response.success(function (data) {
						$log.debug("data   " + data);

						alertService.showAlertSuccess("Операция завершена успешно.");

					});
					response.error(function (data) {
						alertService.showAlertError(data);
					});
				}

			});
	};

	return changePass;
});