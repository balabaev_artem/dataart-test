/**
 * Created by eretic on 16.06.2016.
 */
mainModule.service("alertService", function(growl, $log){

    this.showAlertSuccess = function (message, config) {
        if (!config) {
            config = {}
        }
        growl.success(message, config);
    };

    this.showAlertError = function (data) {

        var message = "";
        var code = 0;
        var name = "";
        var config = {};

        if(data) {
            message = data.message;
            code = data.errorCode;
            name = data.name;
        } else {
            message = "Error to connect to the server";
        }

        $log.error(name +  ": " + code + ": " + message);
        growl.error(message, config);
    };


    this.showError = function (data) {
        var config = {};
        $log.error(data);
        growl.error(data, config);
    };

});